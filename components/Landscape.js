import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import Animated, { BounceOutUp, Layout }from 'react-native-reanimated';

export default function Landscape({ landscape, setLandscapes }) {
   
   const removeLandscape = () => {
    setLandscapes(currState => {
        return currState.filter(land => {
            if (land.id !== landscape.id) {
                return land;
            }
        })
    })
   }
  
   return (
    <Animated.View style={styles.landscapeContainer} 
        exiting={BounceOutUp}
        layout={Layout.duration(500).delay(500)}
    >
      <Image 
        resizeMode="stretch" 
        style={styles.image}
        source={{ uri: landscape.image }}/>
      <Text>{landscape.place}</Text>
      <TouchableOpacity style={styles.delete} onPress={removeLandscape}>
        <AntDesign name="delete" size={24} color="black" />
      </TouchableOpacity>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 170,
        borderRadius: 25
    },
    delete: {
        position: 'absolute',
        alignSelf: 'flex-end',
        marginVertical: 15,
        paddingHorizontal: 25
    },
    landscapeContainer: {
        paddingHorizontal: 20,
        marginVertical: 5
    }
})