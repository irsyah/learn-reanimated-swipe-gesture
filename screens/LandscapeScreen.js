import { View, Text, FlatList } from 'react-native'
import React, { useEffect, useState } from 'react'

import Landscape from '../components/Landscape'

export default function LandscapeScreen() {
  const [ landscapes, setLandscapes ] = useState([])

  useEffect(() => {
    setLandscapes([ {
        image: 'https://wallpaperaccess.com/full/21964.jpg',
        place: 'Gunung Bromo, Malang',
        id: 1
    }, 
    {
        image: 'https://img.freepik.com/free-photo/pura-ulun-danu-bratan-hindu-temple-with-boat-bratan-lake-landscape-sunrise-bali-indonesia_29505-914.jpg?size=626&ext=jpg',
        place: 'Pura Ulun Danu Bratan, Bali',
        id: 2
    }, 
    {
        image: 'https://media.istockphoto.com/photos/mountain-range-in-komodo-national-park-in-indonesia-picture-id472831484?b=1&k=20&m=472831484&s=170667a&w=0&h=aN54YHYnQV32TQ79CNp3zlb17XJYo8yG3mu5TbbqDso=',
        place: 'Labuan Bajo, Kupang',
        id: 3
    }, 
    {
        image: 'https://www.jababekamorotai.com/wp-content/uploads/2019/07/pulau-dodola-819x1024.jpg',
        place: 'Pantai Dodola, Morotai',
        id: 4
    }, 
    {
        image: 'https://www.rinjaninationalpark.com/wp-content/uploads/2016/09/gunungrinjanilombok21.jpg',
        place: 'Gunung Rinjani, Lombok',
        id: 5
    }])
  }, [])
  return (
    <View>
      <Text>LandscapeScreen</Text>
      <FlatList 
        data={landscapes}
        renderItem={({item}) => (
            <Landscape landscape={item} setLandscapes={setLandscapes}/>
        )}
        keyExtractor={(item) => item.id}
      />

    </View>
  )
}