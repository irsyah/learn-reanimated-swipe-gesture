import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { GestureHandlerRootView, PanGestureHandler, PinchGestureHandler, Swipeable } from 'react-native-gesture-handler'
import Animated, { useSharedValue, useAnimatedStyle, useAnimatedGestureHandler } from 'react-native-reanimated'
import { Entypo } from '@expo/vector-icons';

export default function GestureScreen() {

    const transX = useSharedValue(0);
    const transY = useSharedValue(0);
    const scale = useSharedValue(1);

    const AnimatedImage = Animated.createAnimatedComponent(Image)

    const animatedStyle = useAnimatedStyle(() => ({
        transform: [
            { translateX: transX.value },
            { translateY: transY.value }
        ]
    }))

    const pinchAnimatedStyle = useAnimatedStyle(() => ({
        transform: [
            { scale: scale.value }
        ]
    }))

  // GESTURE HANDLER 
  const panHandler = useAnimatedGestureHandler({
    onActive: (event) => {
        transX.value = event.translationX;
        transY.value = event.translationY
    }
  })

  const pinchHandler = useAnimatedGestureHandler({
    onActive: (event) => {
        scale.value = event.scale;
    }
  })

  const RightSwipeAction = () => {
    return (
        <View style={styles.remove}>
            <TouchableOpacity onPress={remove}>
                <Entypo name="trash" size={24} color="black" />
            </TouchableOpacity>
        </View>
    )
  }

  const remove = () => {
    console.log("mau remove")
  }

  return (
    <View>
        <GestureHandlerRootView>
            <PanGestureHandler onGestureEvent={panHandler}>
                <AnimatedImage resizeMode='stretch' style={[ styles.image, animatedStyle]} source={{ uri: "https://wallpaperaccess.com/full/21964.jpg"}}/>
            </PanGestureHandler>

            <PinchGestureHandler onGestureEvent={pinchHandler}>
                <AnimatedImage resizeMode='stretch' style={[ styles.image, pinchAnimatedStyle]} source={{ uri: "https://www.jababekamorotai.com/wp-content/uploads/2019/07/pulau-dodola-819x1024.jpg"}}/>
            </PinchGestureHandler>
        </GestureHandlerRootView>

        <Swipeable renderRightActions={RightSwipeAction}>
            <Image resizeMode='stretch' style={styles.image} source={{ uri: "https://www.rinjaninationalpark.com/wp-content/uploads/2016/09/gunungrinjanilombok21.jpg"}}></Image>
        </Swipeable>
    </View>
  )
}

const styles = StyleSheet.create({
    image: {
        width: 200,
        height: 200
    },
    remove: {
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    }
})
