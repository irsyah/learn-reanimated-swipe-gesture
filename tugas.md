Buatlah aplikasi RN menggunakan Expo. Berikut requirement app:

1. Buatlah Menu menggunakan Tab dengan menu
    - Home
    - Favorites

2. Pada menu Home tampilkan:
    - list (10) image dari shibe inu  
    - Tombol "Generate" untuk me-random kembali 10 image terbaru
    - Data image shibe inu didapatkan dari public API berikut: https://shibe.online/
    - Pada tiap image, dapat melakukan double tap dan data image tersebut akan masuk ke Store Redux(silakan dipikirkan state untuk menyimpan favorites bertipe apa)

3. Pada menu Favorites tampilkan:
    - data image shibe inu yang ada pada state yang ada pada Store Redux
    - pada tiap image di menu Favorites bisa di Swipe untuk melakukan delete
    - data image pada state Store akan terhapus 


# Deadline

Kamis, pukul 23:59

# Cara Pengumpulan

- Buat repo/project private dengan nama reanimated-favorites-shiba-[namaKamu]
- add "irsyah" sebagai role developer dengan masa expired sampai 15 Des 2022
